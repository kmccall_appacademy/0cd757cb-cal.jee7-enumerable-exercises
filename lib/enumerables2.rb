require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  arr.reduce(0, &:+)
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.all? { |string| is_substring?(string, substring) }
end

def is_substring?(str, sub_str)
  str.include?(sub_str)
end


# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  string.delete!(" ")
  string.chars.uniq.sort.reduce([]) do |acc, char|
    if string.count(char) > 1
      acc << char
    else
      acc
    end
  end
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  sorted_string = string.downcase.gsub(/[^a-z0-9\s]/i, '').split.sort_by { |word| word.length }
  [sorted_string[-1], sorted_string[-2]]
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  ("a".."z").to_a.reject { |letter| string.chars.include?(letter) }
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  (first_yr..last_yr).to_a.select { |year| not_repeat_year?(year) }
end

def not_repeat_year?(year)
  year.to_s.chars.length == year.to_s.chars.uniq.length
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  songs.select { |song| no_repeats?(song, songs) }.uniq
end

def no_repeats?(song_name, songs)
  prev_song = songs[0]
  prev_song_idx = 0
  songs[1..-1].each_with_index do |song, idx|
    if song_name == song
      if song_name == prev_song && idx - prev_song_idx == 1
        return false
      end
    end
    prev_song = song
    prev_song_idx = idx
  end
  true
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  words = remove_punctuation(string).split
  c_distances = words.map { |word| c_distance(word) }
  min_distance = c_distances.reject{ |distance| distance == -1 }.min
  words[c_distances.index(min_distance)]
end

def c_distance(word)
  word.reverse.index("c") || -1
end

def remove_punctuation(string)
  string.downcase.gsub(/[^a-z0-9\s]/i, '')
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  ranges = []
  start_idx = nil
  arr.each_with_index do |num, idx|
    next_num = arr[idx + 1]
    if num == next_num
      start_idx = idx unless start_idx
    elsif start_idx
      ranges << [start_idx, idx]
      start_idx = nil
    end
  end
  ranges
end
